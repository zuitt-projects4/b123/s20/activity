
/*
	we can also have an array of objects. 

	we can group together objects in an array. 
	That array can also use array methods for the objects.
	However, being objects are more complex than strings or numbers, there is a difference when handling them.

	much like a regular array, we can actually access this array of objects the same way
*/

let users = [

	{
		name: "Mike Shell", 
		username: "mikeBlueShell", 
		email: "mikeyShell01@gmail.com",
		password: "iammikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"
	}, 
	{
		name: "Jake Janella", 
		username: "jjnella99", 
		email: "jakejanella_99@gmail.com",
		password: "jakiejake12",
		isActive: true,
		dateJoined: "January 14, 2015"
	}
]
console.log(users[0])
//How can we access the properties of our objects in an array?

//show the second items' email  property:
console.log(users[1].email)

//Can we update the properties of objects in an array? yes

//mini_act
users[0].email = "mikeKingOfShells@gmail.com"
users[1].email = "janellajakeArchitect@gmail.com"
console.log(users)

//Can we also use array methods for array of objects?
//Add another user into the array:

users.push({

	name:"James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isActive: true,
	dateJoined: "February 14, 2000"
})

class User{
	constructor(name, username, email, password){
		this.name = name
		this.username = username
		this.email = email
		this.password = password
		this.isActive = true
		this.dateJoined = "September 28, 2021"
	}
}

let newUser1 = new User("Kate Middletown", "notTheDutchess", "seriouslyNotDuchess@gmail.com", "notRoyaltyAtAll")

console.log(newUser1)

users.push(newUser1)

console.log(users)

//find()
function login(username, password){
	/*
		check if the username does exist or any user uses that username.
		check if the password given matches the password of our user in the array.
	*/
	let userFound = users.find((user)=>{

		//user is a parameter which receives each items in the users array and the users array is an array of objects. Therefore, we can expect that the user parameter will contain an object.

		return user.username === username && user.password === password
	})
	console.log(userFound)
	//mini_act
	if (userFound){
		alert(`Thank you for logging in, ${userFound.username}`)
	}else{
		alert(`Login Failed. Invalid Credentials.`)
	}
}

let courses = []

//create

const postCourse = (id, name, description , price) => {
	
	 courses.push({

	 	id: id, 
	 	name: name, 
	 	description: description , 
	 	price: price, 
	 	isActive:true

	 })

	alert(`You have created ${name}. The price is ${price}`)
}


//retrieve/read

const getSingleCourse = (id) => {
	
	let courseFound = courses.find((course) => course.id === id)
	return(courseFound)// save the value 
}


//delete
const deleteCourse = () => courses.pop()











